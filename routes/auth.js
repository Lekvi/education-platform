const {Router} = require('express')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('config')
const {check, validationResult} = require('express-validator')
const User = require('../models/User')
const router = Router()


router.post(
    '/registration',
    [
        check('firstName', 'Введите своё имя').exists(),
        check('secondName', 'Введите свою фамилию').exists(),
        check('teacher', 'Выберите своего учителя').exists(),
        check('login', 'Максимальная длина логина 10 символов').exists().isLength({max: 10}),
        check('password', 'Минимальная длина пароля 6 символов').isLength({min: 6})
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Некорректные данные при регистрации'
                })
            }
            const {firstName, secondName, post, teacher, password, login} = req.body

            const newLogin = await User.findOne({login}),
                newName = await User.findOne({firstName}),
                newSurname = await User.findOne({secondName});

            if (newName && newSurname) {
                return res.status(400).json({message: `Пользователь "${secondName} ${firstName}" уже существует`})
            } else if (newLogin) {
                return res.status(400).json({message: `Пользователь с логином "${login}" уже существует`})
            }

            const hashedPassword = await bcrypt.hash(password, 12)

            let user = null
            if (post === 'teacher') {
                user = new User({firstName, secondName, post, login, password: hashedPassword})
            } else {
                user = new User({firstName, secondName, post, teacher, login, password: hashedPassword})
            }

            await user.save()

            res.status(201).json({message: `Пользователь c именем ${login} создан`})

        } catch (e) {
            res.status(500).json({message: 'Попробуйте снова'})
        }
    })

router.post(
    '/login',
    [
        check('login', 'Введите логин').exists(),
        check('password', 'Введите пароль').exists()
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req)

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Некорректные данные при авторизации'
                })
            }

            const {login, password} = req.body

            const user = await User.findOne({login})

            console.log('В системе авторизован пользователь: ', user)

            if (!user) {
                return res.status(400).json({message: `Пользователь с логином ${login} не найден`})
            }

            const isPasMatch = await bcrypt.compare(password, user.password)

            if (!isPasMatch) {
                return res.status(400).json({message: 'Неверный пароль'})
            }

            const token = jwt.sign(
                {userId: user.id},
                config.get('jwtSecret'),
                {expiresIn: '1h'}
            )

            const post = user.post

            console.log(post)

            res.json({token, userId: user.id, login, post})

        } catch (e) {
            res.status(500).json({message: 'Попробуйте снова'})
        }
    })

module.exports = router