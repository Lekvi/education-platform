import React from 'react';
import {
    BrowserRouter as Router
} from "react-router-dom";
import {useAuth} from "./hooks/auth.hook";

import {useRoutes} from "./routes";

import Navbar from "./components/Navbar/Navbar";

import 'materialize-css'
import {AuthContext} from "./context/AuthContext";

function App() {
    const {login, logout, token, userId, userLogin, post} = useAuth()
    const isAuthenticated = !!token;
    const routes = useRoutes(isAuthenticated)

    /*

    Until, for comfort developing

    User : admin
    Password : 123456

    Should be:
    const isAuthenticated = !!token;

    */

    return (
        <AuthContext.Provider value={{
            login, logout, token, userId , userLogin, post, isAuthenticated
        }}>
            <Router>
                {isAuthenticated && <Navbar/>}
                <div className="container">
                    {routes}
                </div>
            </Router>
        </AuthContext.Provider>
    )
}

export default App;
