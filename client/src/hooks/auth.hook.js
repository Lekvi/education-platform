import {useState, useCallback, useEffect} from 'react'

const storageName = 'userData'

export const  useAuth = () => {
    const [token, setToken] = useState(null)
    const [userId, setUserId] = useState(null)
    const [userLogin, setUserLogin] = useState(null)
    const [userPost, setUserPost] = useState(null)


    const login = useCallback((jwtToken, id, login, post) => {
        setToken(jwtToken)
        setUserId(id)
        setUserLogin(login)
        setUserPost(post)

        localStorage.setItem(storageName, JSON.stringify({
            userId: id, token: jwtToken, userLogin: login, userPost: post
        }))
    }, [])

    const logout = useCallback(() => {
        setToken(null)
        setUserId(null)
        setUserLogin(null)
        setUserPost(null)
        localStorage.removeItem(storageName)
    }, [])

    useEffect(() => {
        const data = JSON.parse(localStorage.getItem(storageName))

        if (data && data.token) {
            login(data.token, data.userId, data.userLogin, data.userPost)
        }
    }, [login])

    return {login, logout, token, userId, userLogin, userPost}
}