import React from 'react'
import {
  Switch, Route, Redirect
} from 'react-router-dom'
import {MainPage} from "./pages/MainPage/MainPage";
import {StatisticPage} from "./pages/StatistickPage/StatisticPage";
import {AccountPage} from "./pages/AccountPage/AccountPage";
import {AuthPage} from "./pages/AuthPage/AuthPage";
import {RegisterPage} from "./pages/RegisterPage/RegisterPage";
import VariantPage from "./pages/VariantPage/VariantPage";
import VariantsListPage from "./pages/VariantsListPage/VariantsListPage";

export const useRoutes = (isAuthenticated) => {
  if (isAuthenticated) {
    return (
        <Switch>
          <Route path="/" exact>
            <MainPage/>
          </Route>
          <Route path="/statistics">
            <StatisticPage/>
          </Route>
          <Route path="/variants">
            <VariantsListPage/>
          </Route>
          <Route path="/variant">
            <VariantPage/>
          </Route>
          <Route path="/account">
            <AccountPage/>
          </Route>
          <Redirect to="/"/>
        </Switch>
    )
  }
  return (
      <Switch>
        <Route path="/" exact>
          <AuthPage/>
        </Route>
        <Route path="/registration">
          <RegisterPage/>
        </Route>
        <Redirect to="/"/>
      </Switch>
  )
}