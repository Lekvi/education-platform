import React, {useContext, useEffect, useState} from 'react'
import {NavLink} from "react-router-dom";

import {useHttp} from "../../hooks/http.hook";
import {useMessage} from '../../hooks/message.hook'
import {AuthContext} from "../../context/AuthContext";

import './AuthPage.css'

export const AuthPage = () => {
    const auth = useContext(AuthContext)
    const message = useMessage()
    const {loading, request, error, clearError} = useHttp()
    const [form, setForm] = useState({
        login: '',
        password: ''
    })

    useEffect(() => {
        message(error)
        clearError()
    }, [error, message])

    const changeHandler = event => {
        setForm({ ...form, [event.target.name]: event.target.value})
    }

    //TODO При клике по регистрации открывать другое модальное (новое)
    
    const loginHandler = async () => {
        if (!form.login) {
            window.M.toast({ html: 'Введите логин'})
            return
        }
        if (!form.password) {
            window.M.toast({ html: 'Введите пароль'})
            return
        }
        try {
            const data = await request('/api/auth/login', 'POST', {
                login: form.login,
                password: form.password
            })

            auth.login(data.token, data.userId, data.login, data.post)
        } catch (e) {}
    }

    return (

        //todo Добавить кнопку "забыли логин\пароль" которая
        // откроет модалку с подсказкой обратиться к админу(учителю)

        <div className="row">
            <div className="col s6 offset-s3">
                <h2>Education Platform</h2>
                <div className="card blue darken-1">
                    <div className="card-content white-text">
                        <span className="card-title">Авторизация</span>
                        <div>
                            <div className="input-field">
                                <input
                                    id="login"
                                    type="text"
                                    className="validate yellow-input"
                                    name="login"
                                    required
                                    onChange={changeHandler}
                                />
                                    <label htmlFor="login">Логин</label>
                            </div>
                            <div className="input-field">
                                <input
                                    id="password"
                                    type="password"
                                    className="validate yellow-input"
                                    name="password"
                                    minLength="6"
                                    required
                                    onChange={changeHandler}
                                />
                                <label htmlFor="password">Пароль</label>
                            </div>
                        </div>
                    </div>
                    <div className="card-action">
                        <button
                            className="btn yellow darken-4 login-btn waves-effect waves-light"
                            onClick={loginHandler}
                            disabled={loading}
                        >Войти</button>
                    </div>
                    <NavLink className="ngl btn-floating btn-large waves-effect waves-light red" to="registration">
                        <i className="material-icons">person_add</i>
                    </NavLink>
                </div>
            </div>
        </div>
    )
}