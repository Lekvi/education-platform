import React, {useEffect, useState} from 'react'
import {connect} from 'react-redux'
import {NavLink} from "react-router-dom";

import './VarianPage.css'

const VariantPage = ({variant, variantNumber}) => {
  const initialAnswers = {
    "1": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "26"
    },
    "2": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "xwyz"
    },
    "3": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "2186"
    },
    "4": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "23"
    },
    "5": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "35"
    },
    "6": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "67"
    },
    "7": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "100"
    },
    "8": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "180"
    },
    "9": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "34"
    },
    "10": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "1729"
    },
    "11": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "240"
    },
    "12": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "11222"
    },
    "13": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "10"
    },
    "14": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "3"
    },
    "15": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "16"
    },
    "16": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "164"
    },
    "17": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "1568 7935"
    },
    "18": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "1204 502"
    },
    "19": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "13"
    },
    "20": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "20 24"
    },
    "21": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "0"
    },
    "22": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "130"
    },
    "23": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "627"
    },
    "24": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "784"
    },
    "25": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "3 58153 7 24923 59 2957 13 13421 149 1171 5 34897 211 827 2 87251"
    },
    "26": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "482303 576"
    },
    "27": {
      "result": "incorrect",
      "userAnswer": "-",
      "taskAnswer": "18380 58701760"
    },
    "variant": 1,
    "correct": 0,
    "incorrect": 27
  }

  const [answers, setAnswers] = useState(initialAnswers)
  const [results, setResults] = useState(null)
  const checkVariant = () => { // проверка результатов
    let results = {
      variant: variantNumber,
      correct: 0,
      incorrect: 0
    }
    variant.forEach(task => {
      console.log(task)
      Object.keys(answers).forEach(key => {
        if (task.blockNumber === key) {
          if (answers[key] === task.taskAnswer) {
            // results = {...results, [key]: 'correct'}
            let i = results.correct
            i++
            let resultInfo = {
              result: 'correct',
              userAnswer: answers[key],
              taskAnswer: task.taskAnswer
            }
            results = {...results, [key]: resultInfo, correct: i}
          } else {
            // results = {...results, [key]: 'incorrect'}
            let i = results.incorrect
            i++
            let resultInfo = {
              result: 'incorrect',
              userAnswer: answers[key],
              taskAnswer: task.taskAnswer
            }
            results = {...results, [key]: resultInfo, incorrect: i}
          }
        }
      })
    })
    setResults(results)
  }

  const answerHandler = (e) => {

    let userAnswer = e.target.value,
        blockNumber = e.target.id;
    setAnswers({...answers, [blockNumber]: userAnswer.trim()})
  }

  useEffect(() => {
    console.log('РЕЗУЛЬТАТЫ ПРОВЕРКИ ', results)
    //  TODO наверное тут нужно записывать результаты проверки в бек?
  }, [results])

  return (
      <>
        <div className="row">
          {
            variantNumber ? <h1 className="col s8 m8 l8">Вариант {variantNumber}</h1> :
                <h1>Что то пошло не так ...</h1>
          }
        </div>
        {
          variant && variant.map(task => {
            return (
                task.blockNumber !== 20 && task.blockNumber !== 21 ?
                    <div className="col s12 m12 l12" key={task.blockNumber}>
                      <div className="card blue darken-1">
                        <div className="card-content white-text">
                          {
                            task.blockNumber !== 19 ?
                                <span className="card-title yellow-text">{task.blockNumber}</span> :
                                <span className="card-title yellow-text">19 20 21</span>
                          }
                          <div dangerouslySetInnerHTML={{__html: task.taskDescriptions}}></div>
                        </div>
                        <div className="card-action">
                          <label htmlFor={task.blockNumber}>Ваш ответ</label>
                          <div className="input-field">
                            {
                              task.blockNumber !== 19 ?
                                  <input
                                      id={task.blockNumber}
                                      onInput={answerHandler}
                                  /> :
                                  <>
                                    <input
                                        id="19"
                                        onInput={answerHandler}
                                    />
                                    <input
                                        id="20"
                                        onInput={answerHandler}
                                    />
                                    <input
                                        id="21"
                                        onInput={answerHandler}
                                    />
                                  </>
                            }
                          </div>
                        </div>
                      </div>
                    </div> : null
            )
          })
        }
        {
          variant ?
              <div className="row variants-actions">
                <NavLink
                    to="/variants"
                    className="col s4 m4 l4 waves-effect waves-light btn">Вернуться
                </NavLink>
                <a className="col s4 m4 l4 waves-effect waves-light btn"
                   onClick={checkVariant}
                >На проверку</a>
              </div> :
              <div className="row">
                <NavLink
                    to="/variants"
                    className="col s4 m4 l4 waves-effect waves-light btn">Вернуться
                </NavLink>
              </div>
        }
      </>
  )
}

const mapStateToProps = state => {
  return {
    variant: state.variants.chosenVariant,
    variantNumber: state.variants.chosenVariantNumber
  }
}

export default connect(mapStateToProps, null)(VariantPage)