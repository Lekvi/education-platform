import React from 'react'
import {connect} from 'react-redux'
import {chooseVariant, chooseVariantNumber} from "../../redux/actions";
import {ReactComponent as EducationSVG} from "../../assets/education.svg";

import './VariantsPage.css'

import {NavLink} from "react-router-dom";

const VariantsListPage = ({chooseVariant, chooseVariantNumber, allVariants}) => {

  const setVariantData = (e) => {
    let variantNumber = +(e.target.text),
        index = +(e.target.text) - 1
    chooseVariantNumber(variantNumber)
    chooseVariant(allVariants[index])
  }
  return (
      <>
        <div className="variants-page">
          <div className="row ">
            <div className="col s12 m12 l12">
              <div className="card white">
                <div className="card-content blue-text">
                  <div>
                    <h3>Тренировочные варианты </h3>
                    <p className="card-description">Все задания очень похожи на те, что используются на настоящем
                      экзамене.
                      Они подходят для подготовки к ЕГЭ-2021. Их составляют эксперты.</p>
                    <h5>Список: </h5>
                    <div className="variants">
                      {
                        allVariants && allVariants.map((variant, i) => {
                          return (
                              <>
                                <NavLink
                                    to="/variant"
                                    onClick={setVariantData}
                                    className="variant variant-1"
                                >{i + 1}
                                </NavLink>
                              </>
                          )
                        })
                      }
                    </div>
                  </div>
                  <div className="img">
                    <EducationSVG className="educationSvg"/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
  )
}

const mapDispatchToProps = {
  chooseVariant, chooseVariantNumber
};

const mapStateToProps = state => {
  return {
    allVariants: state.variants.allVariants
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VariantsListPage)

