import React, {useEffect, useState} from 'react'
import {NavLink, useHistory } from "react-router-dom";

import {useHttp} from "../../hooks/http.hook";
import {useMessage} from "../../hooks/message.hook";

import M from 'materialize-css'
import './RegisterPage.css'


export const RegisterPage = () => {
    const message = useMessage()
    const history = useHistory()
    const {loading, request, error, clearError} = useHttp()
    const [form, setForm] = useState({
        firstName: '',
        secondName: '',
        teacher: '',
        login: '',
        password: ''
    })
    const teacherSelectTag = React.createRef()

    useEffect(() => {
        message(error)
        clearError()
    }, [error, message])

    const changeHandler = event => {
        if (event.target.localName === 'select') {
            /*console.log(event.target.id)
            console.log(event.target[event.target.selectedIndex].text)*/
            setForm({...form, [event.target.id]: event.target[event.target.selectedIndex].text})
        } else {
            setForm({...form, [event.target.name]: event.target.value})
        }
    }

    const registerHandler = async () => {
        if (!form.firstName) {
            window.M.toast({ html: 'Введите имя'})
            return
        }
        if (!form.secondName) {
            window.M.toast({ html: 'Введите фамилию'})
            return
        }
        if (!form.login) {
            window.M.toast({ html: 'Введите логин'})
            return
        }
        if (!form.password) {
            window.M.toast({ html: 'Введите пароль'})
            return
        }
        if (!form.teacher) {
            window.M.toast({ html: 'Выберите своего учителя'})
            return
        }
        try {
            const data = await request('/api/auth/registration', 'POST', {
                firstName: form.firstName,
                secondName: form.secondName,
                post: 'student',
                teacher: form.teacher,
                login: form.login,
                password: form.password
            })
            if (data && data.message) {
                window.M.toast({ html: data.message})
                setTimeout(() => {
                    history.push('/')
                }, 1000)
            }

        } catch (e) {}
    }

    useEffect(() => {
        const teacherSelect = teacherSelectTag.current
        M.FormSelect.init(teacherSelect)
    }, [])

    //todo проверить как отрабатывает required в input

    return (
        <div className="row">
            <div className="col s8 offset-s3">
                <h2>Education Platform</h2>
                <div className="card blue darken-1">
                    <div className="card-content white-text">
                        <span className="card-title white-text">Регистрация</span>
                        <div className="row">
                            <div className="input-field col s6">
                                <input id="first_name" type="text" className="validate yellow-input"
                                       onChange={changeHandler}
                                       name="firstName"
                                       required/>
                                <label htmlFor="first_name">Имя</label>
                            </div>
                            <div className="input-field col s6">
                                <input id="last_name" type="text" className="validate yellow-input"
                                       onChange={changeHandler}
                                       name="secondName"
                                       required/>
                                <label htmlFor="last_name">Фамилия</label>
                            </div>
                        </div>
                        <div className="input-field">
                            <input id="login" type="text" className="validate yellow-input"
                                   onChange={changeHandler}
                                   name="login"
                                   required/>
                            <label htmlFor="login">Логин</label>
                        </div>
                        <div className="input-field">
                            <input id="password" type="password" className="validate yellow-input"
                                   minLength="6"
                                   onChange={changeHandler}
                                   name="password"
                                   required/>
                            <label htmlFor="password">Пароль</label>
                        </div>
                        <div className="input-field">
                            <select id="teacher"
                                    ref={teacherSelectTag}
                                    onChange={changeHandler}
                                    name="teacher"
                                    required
                                    defaultValue={'DEFAULT'}
                            >
                                <option value="DEFAULT" disabled></option>
                                <option value="1">Елена Евгеньевна</option>
                                <option value="2">Марья Петровна</option>
                                <option value="3">Сан Саныч</option>
                            </select>
                            <label htmlFor="teacher">Преподаватель</label>
                        </div>
                    </div>
                    <div className="card-action">
                        <button className="btn yellow darken-4 waves-effect waves-light"
                                name="action"
                                onClick={registerHandler}
                                disabled={loading}
                        >Зарегистрироваться
                        </button>
                    </div>
                    <NavLink className="ngl btn-floating btn-large waves-effect waves-light red" to="/">
                        <i className="material-icons">person</i>
                    </NavLink>
                </div>
            </div>
        </div>
    )
}