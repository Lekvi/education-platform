import {CHOOSE_VARIANT} from "./types";
import {CHOSEN_VARIANT_NUMBER} from "./types";


export function chooseVariant(variant) {
  return {
    type: CHOOSE_VARIANT,
    payload: variant
  }
}
export function chooseVariantNumber(variantNumber) {
  return {
    type: CHOSEN_VARIANT_NUMBER,
    payload: variantNumber
  }
}

