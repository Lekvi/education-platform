import {combineReducers} from "redux";
import {variantsReducer} from "./variantsReducer";

export const rootReducer = combineReducers({
  variants: variantsReducer
});