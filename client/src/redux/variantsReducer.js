import {CHOOSE_VARIANT} from "./types";
import {CHOSEN_VARIANT_NUMBER} from "./types";

import mockVariants from '../mock-from-web/variants.json'

const initialState = {
  allVariants: mockVariants,
  chosenVariant: [],
  chosenVariantNumber: 1
}

export const variantsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHOOSE_VARIANT:
      return {...state, chosenVariant: action.payload}
    case CHOSEN_VARIANT_NUMBER:
      return {...state, chosenVariantNumber: action.payload}
    default: return state
  }
}