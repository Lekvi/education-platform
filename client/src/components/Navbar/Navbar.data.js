import React from 'react'

export const NavbarLinks = [
    {
        title: 'Варианты',
        path: '/variants'
    },
    {
        title: 'Статистика',
        path: '/statistics'
    }
]