import React, {useContext} from 'react'
import {NavLink} from "react-router-dom";
import {AuthContext} from "../../context/AuthContext";

import {NavbarLinks} from "./Navbar.data";

import logo from "../../assets/react-logo.png"

import M from 'materialize-css'
import './Navbar.css'

function Navbar() {
    const auth = useContext(AuthContext)
    const dropBtn = React.createRef()

    const logout = () => {
        auth.logout()
    }

    function drop() {
        const elem = dropBtn.current
        M.Dropdown.init(elem)
        const instance = M.Dropdown.getInstance(elem);
        instance.open()
    }

    return (
        <>
            <nav>
                <div className="nav-wrapper blue darken-1">
                    <NavLink to="/" className="brand-logo">
                        <img src={logo} alt="main-link" className="reactLogo"/>
                    </NavLink>
                    <ul id="nav-mobile" className="right hide-on-med-and-down">
                        {NavbarLinks.map((item, index) => {
                            return (
                                <li key={index}>
                                    <NavLink to={item.path}>{item.title}</NavLink>
                                </li>
                            )
                        })}
                        <li>
                            <button
                                className='dropdown-trigger btn'
                                data-target='dropdown'
                                onClick={drop}
                                ref={dropBtn}
                            >{auth.userLogin}</button>

                            <ul id='dropdown' className='dropdown-content'>
                                <li><NavLink to="/statistics">Статистика</NavLink></li>
                                <li><NavLink to="/variants">Варианты</NavLink></li>
                                <li><span onClick={logout}>Выйти</span></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </>
    )
}

export default Navbar