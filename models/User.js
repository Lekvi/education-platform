const {Schema, model} = require('mongoose')

const schema = new Schema({
    firstName: { type: String, required: true},
    secondName: { type: String, required: true},
    post: { type: String},
    teacher: { type: String},
    login: { type: String, required: true, unique: true },
    password: { type: String, required: true }
})

module.exports = model('User', schema)